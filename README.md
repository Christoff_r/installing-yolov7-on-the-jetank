# Installing YOLOv7 on the Jetson nano JETANK image

Normally when installing YOLOv7 you can just run the `requirenebts.tx` but since we are on the Jetson nano we need to do it a little different.

## Table of Contents

- [You're Gonna Need a Bigger Boat](#youre-gonna-need-a-bigger-boat)
- [PIP my ride](#pip-my-ride)
- [Install PyTorch and torchvision for Jetson](#install-pytorch-and-torchvision-for-jetson)
- [The part we actually care about](#the-part-we-actually-care-about)
- [Contributing](#contributing)


## You're Gonna Need a Bigger Boat

Despite flashing the image onto a 64G SD card the images only let me use 24G, and occupying 22 of them, only leaving 2G of free space,which is not enough (At least not for this approach). SO we need to make some changes so we can use some more of the space on the SD card.

Now, I am pretty new to this Linux stuff and I do not really know why this works, but this approach let me use almost all the space on the SD, so I take it.

First write:

```
sudo fdisk /dev/mmcblk0
```

Then you will be prompted to enter some command. At the end it should look like this:

```
Command (m for help): d
Partition number (1-14, default 14): 1

Command (m for help): n
Partition number (1,15-128, default 1): [Press enter]
First sector (34-123596766, default 28672): [Press enter]
Last sector, +sectors or +size{K,M,G,T,P} (28672-123596766, default 123596766): [Press enter]
Do you want to remove the signature? [Y]es/[N]o: N

Command (m for help): w 
```

Then expand the filesystem

```
sudo resize2fs /dev/mmcblk0p1
```
Check the available space:

```
df -h
```

This should output something like this:

```
Filesystem          Size   Used  Avail  Use% Mounted on
/dev/mmcblk0p1  58G   22G   35G    39% /
```


## PIP my ride

First thing to do is clone the YOlOv7 repository

```
git clone https://github.com/WongKinYiu/yolov7.git
```

Then we need to install some dependencies. In my case I also need to install `sudo`. If `sudo` works for you just skip the first line.

```
apt install sudo

sudo apt install python3-pip

pip3 install -U PyYAML

pip3 install tqdm

pip3 install cython

pip3 install numpy==1.19.5

sudo apt install libjpeg-dev

pip3 install matplotlib

sudo apt install gfortran

sudo apt install libopenblas-dev

sudo apt install liblapack-dev

pip3 install scipy

pip3 install seaborn

pip3 install typing-extensions
```

## Install PyTorch and torchvision for Jetson

We need PyTorch that is built for the Jetson specific.

```
wget https://nvidia.box.com/shared/static/p57jwntv436lfrd78inwl7iml6p13fzh.whl -O torch-1.8.0-cp36-cp36m-linux_aarch64.whl

sudo apt-get install python3-pip libopenblas-base libopenmpi-dev libomp-dev

pip3 install torch-1.8.0-cp36-cp36m-linux_aarch64.whl

sudo apt-get install libjpeg-dev zlib1g-dev libpython3-dev libavcodec-dev libavformat-dev libswscale-dev

git clone --branch v0.9.0  https://github.com/pytorch/vision torchvision

cd torchvision

export BUILD_VERSION=0.9.0

python3 setup.py install --user

cd ../
```

## The part we actually care about

We need a version of YOLOv7 built to run on edge computers.

```
cd yolov7/

wget https://github.com/WongKinYiu/yolov7/releases/download/v0.1/yolov7-tiny.pt
```

Then test if it works by running the following line.

```
python3 detect.py --weights yolov7-tiny.pt --source inference/images/horses.jpg
```
You should get the followinf output:

```
Fusing layers... 
Model Summary: 200 layers, 6219709 parameters, 229245 gradients
 Convert model to Traced-model... 
 traced_script_module saved! 
 model is traced! 

5 horses, Done. (167.5ms) Inference, (137.5ms) NMS
 The image with the result is saved in: runs/detect/exp2/horses.jpg
Done. (1.167s)
```

And an image looking something like this:

![horses detected by the YOLOv7](/imgs/horses.jpg)

You now have YOLOv7 on your Jetson nano JETANK!

## Contributing

If you have any tips, sugestions or things like that feel free to send me a message :speech_balloon:, although I might me slow to respond :sweat_smile: